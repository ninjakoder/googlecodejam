package Y2016.qualification;

import java.util.LinkedHashSet;
import java.util.List;

import Y2016.qualification.revengeofpancakes.RevengeOfThePancakes;

/**
 * Created by aatmapremaarya on 09/04/16.
 */
public class Test {

    @org.junit.Test
    public void testGetDivisionsFromRight(){

        String stack = "+-+";
        List<Integer> list = RevengeOfThePancakes.getDivisionsFromRight(stack);
        for (int i : list){
            System.out.println(RevengeOfThePancakes.reverse(stack.substring(0,i)) + stack.substring(i,stack.length()));
            System.out.println(stack.length());
        }

        //System.out.println(list);

    }

    @org.junit.Test
    public void testReverse(){
        System.out.println(RevengeOfThePancakes.reverse("-+++++-+--"));
    }

    @org.junit.Test
    public void testGetMinFlips(){
        System.out.println(RevengeOfThePancakes.getMinFlips("-++-+-++--+-+-++++++--+-++-++-++----++-----+----+---+++-----+-+++-+--+----------+------+++------+--+",0, new LinkedHashSet<String>(), Integer.MAX_VALUE));
    }
}
