package Y2016.qualification.coinjam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by aatmapremaarya on 09/04/16.
 */
public class CoinJam {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        long j,n;
        for (int i = 0; i < t; i++) {
            n = sc.nextLong();
            j = sc.nextLong();

            generateJCoinsOfLengthL(j,n);
        }
    }

    public static void generateJCoinsOfLengthL(long j , long n){

        List<String> patterns = getAllCombindations("",n-2);
        String pattern;

        int p = 1;
        int z = 1;
        PrimalityAndDivisor[] pdArr = new PrimalityAndDivisor[11];

        System.out.println("Case #"+z+":");
        while (z <= j && p < patterns.size()){
            pattern = append1beforeAfter(patterns.get(p++));
            boolean noPrimesInAnyBase = true;
            for (int k = 2 ; k <= 10 ; k++){
                pdArr[k] = isPrime(Long.valueOf(pattern,k));
                if(pdArr[k].isPrime()){
                    noPrimesInAnyBase = false;
                    break;
                }
            }

            if(noPrimesInAnyBase){
                System.out.print(pattern);

                for (int k = 2; k <=10 ; k++) {
                    System.out.print(" "+pdArr[k].getDivisor());
                }
                System.out.println();
                z++;

            }
        }

    }

    static class PrimalityAndDivisor{
        boolean isPrime;
        int divisor;

        public PrimalityAndDivisor(boolean isPrime, int divisor) {
            this.isPrime = isPrime;
            this.divisor = divisor;
        }

        public int getDivisor() {
            return divisor;
        }

        public boolean isPrime() {
            return isPrime;
        }


    }

    public static String append1beforeAfter(String pattern){
        return "1"+pattern+"1";
    }

    public static List<String> getAllCombindations(String pattern, long length){
        List<String> result = new ArrayList<String>();
        if(length == 0){
            result.add(pattern);
        }else {
            result.addAll(getAllCombindations(pattern+"0",length-1));
            result.addAll(getAllCombindations(pattern+"1",length-1));
        }
        return result;
     }

    public static PrimalityAndDivisor isPrime(long n) {
        PrimalityAndDivisor result = new PrimalityAndDivisor(true,0);

        if (n <= 1) {
            result = new PrimalityAndDivisor(true,0);
        }
        for (int i = 2; i < Math.sqrt(n); i++) {
            if (n % i == 0) {
                result = new PrimalityAndDivisor(false,i);
            }
        }

        return result;
    }


}
