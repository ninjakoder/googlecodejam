package Y2016.qualification.coinjam;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * Created by aatmapremaarya on 09/04/16.
 */
public class Driver
{
    /* args[0] - class to launch, args[1]/args[2] file to direct System.out/System.err to */
    public static void main(String[] args) throws Exception
    {
        String path = "/Users/aatmapremaarya/Documents/myrepo/googlecodejam/src/main/java/Y2016/qualification/coinjam/";
        String name = "C-large";

        System.setIn(new FileInputStream(new File(path+name+".in")));
        System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(path+name+".out")),true));

        String[] cargs={};
        CoinJam.main(cargs);

//        Class app = Class.forName("CountingSheepSolution");
//        Method main = app.getDeclaredMethod("main", new Class[] { (new String[1]).getClass()});
//        String[] appArgs = new String[args.length-3];
//        System.arraycopy(args, 3, appArgs, 0, appArgs.length);
//        main.invoke(null, appArgs);
    }
}
