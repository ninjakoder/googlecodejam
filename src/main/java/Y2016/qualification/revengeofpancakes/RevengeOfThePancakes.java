package Y2016.qualification.revengeofpancakes;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by aatmapremaarya on 09/04/16.
 */
public class RevengeOfThePancakes {

    public static ConcurrentHashMap<String, Integer> globalMap = new ConcurrentHashMap<String, Integer>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int noOfTestCases = sc.nextInt(); sc.nextLine();
        for (int i = 0; i < noOfTestCases; i++) {
            String testCase = sc.nextLine().trim();
            //System.out.println("*********************");
            //System.out.println("Input: "+testCase );
            globalMap = new ConcurrentHashMap<String, Integer>();
            System.out.println("Case #" + (i + 1) + ": " + getMinFlips(testCase, 0, new LinkedHashSet<String>(), Integer.MAX_VALUE));
            //System.out.println("*********************");
        }
    }

    public static int getMinFlips(String stack, int count, LinkedHashSet<String> parent, int minSoFar){
        if(isDone(stack) || count > minSoFar){
            //System.out.println(parent);
            return count;
        }
        else if(globalMap.containsKey(stack)){
            return globalMap.get(stack);
        }
        else {
            List<Integer> divisionsFromRight = getDivisionsFromRight(stack);
            LinkedHashSet<String> current = new LinkedHashSet<String>(parent);
            current.add(stack);

            int minflips = Integer.MAX_VALUE;

            for(int j = divisionsFromRight.size()-1 ; j >= 0 ;j--){
                int i = divisionsFromRight.get(j);
                String reverseSubStack = reverse(stack.substring(0,i)) + stack.substring(i,stack.length());

                if(!current.contains(reverseSubStack)){
                    current.add(reverseSubStack);
                    int flips = getMinFlips(reverseSubStack, count+1, current, minflips);
                    minflips = minflips < flips ? minflips : flips;
                }
            }

            globalMap.put(stack,minflips);
            return minflips;
        }
    }

    static boolean isDone(String stack){
        boolean result = true;
        for (int i = 0; i < stack.length() ; i++) {
            if(stack.charAt(i)!='+'){
                result = false;
                break;
            }
        }
        return result;
    }

    public static String reverse(String subStack){
        StringBuilder sb = new StringBuilder();
        for (int i = subStack.length()-1; i >= 0  ; i--) {
            sb.append(subStack.charAt(i)=='+'?'-':'+');
        }
        return sb.toString();
    }

    public static List<Integer> getDivisionsFromRight(String stack){
        List<Integer> divisionsFromRight = new ArrayList<Integer>();
        Character temp = 'A';
        for (int i = stack.length()-1 ; i >= 0 ; i--) {
            if (stack.charAt(i)!= temp){
                divisionsFromRight.add(i+1);
                temp = stack.charAt(i);
            }
        }

        return divisionsFromRight;
    }
}

