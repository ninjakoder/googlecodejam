package Y2016.qualification.fractiles;

import static javafx.scene.input.KeyCode.K;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aatmapremaarya on 10/04/16.
 */
public class Fractiles {

    public static void main(String[] args) {

    }

    public static String getIndexOfStones(int k, int c , int s){
        int[] totalFractalScores = getTotalScoreOfAllFractals(k,c);

        for (int i = 0; i < totalFractalScores.length; i++) {

        }
    }

    public static boolean[] getTotalScoreOfAllFractals(int k, int c){

        List<String> baseFractals = getAllBaseFractals("",k);

        boolean[] totalFractalScores = new boolean[k*c];
        for (int i = 0; i < (k*c); i++) {
            totalFractalScores[i]=false;
        }

        for(String baseFractal : baseFractals){
            if(containsGold(baseFractal)){
                totalFractalScores = and(totalFractalScores,getFractalScores(baseFractal,k,c));
            }

        }

        return totalFractalScores;
    }

    public static boolean containsGold(String fractal){
        boolean result = true;
        for (int i = 0; i < fractal.length(); i++) {

        }
    }

    public static boolean[] and(boolean[] total, boolean[] add){
        for (int i = 0; i < total.length; i++) {
            total[i]=total[i] && add[i];
        }

        return total;
    }

    public static int[] getFractalScores(String fractalPattern, int k, int c){
        if(c==0){
            return getScore(fractalPattern);
        }else {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < fractalPattern.length(); i++) {
                if(fractalPattern.charAt(i)=='L'){
                    sb.append(fractalPattern);
                }else {
                    for (int j = 0; j < k; j++) {
                        sb.append('G');
                    }
                }
            }

            return getFractalScores(sb.toString(),k,c-1);
        }
    }

    public static int[] getScore(String fractal){
        int[] result = new int[fractal.length()];
        for (int i = 0; i < fractal.length(); i++) {
            result[i]=fractal.charAt(i)=='L'?1:0;
        }
        return result;
    }

    public static List<String> getAllBaseFractals(String fractal, int k){
        List<String> result = new ArrayList<String>();
        if(k==0){
            result.add(fractal);
        }else {
            result.addAll(getAllBaseFractals(fractal+"G",k-1));
            result.addAll(getAllBaseFractals(fractal+"L",k-1));
        }
        return result;
    }
}
