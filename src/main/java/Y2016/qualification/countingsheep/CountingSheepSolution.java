package Y2016.qualification.countingsheep;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.InputMismatchException;

/**
 * Created by aatmapremaarya on 09/04/16.
 */
public class CountingSheepSolution {

    public static void main(String[] args) {
        FastReader fr = new FastReader(new BufferedInputStream(System.in));
        //FastWriter fw = new FastWriter(outputStream);

        int noOfInputSets = fr.nextInt();

        long number;
        for (int i = 0; i < noOfInputSets; i++) {
            number = fr.nextLong();
            CountingSheep cs = new CountingSheep(number);
            System.out.println("Case #"+(i+1)+": " + cs.countToSleep());
        }

        //fw.close();
    }

    public static class CountingSheep{
        public static final String INSOMNIA = "INSOMNIA";
        public static final int SUCCESS = 0;
        public static final int FAIL = -1;

        long number;
        HashSet<Integer> digits;

        public CountingSheep(long number){
            this.number = number;
            digits = new HashSet<Integer>();
            for (int i = 0; i < 10; i++) {
                digits.add(i);
            }
        }

        public String countToSleep(){
            String result = INSOMNIA;

            long currentValue;
            for (int i = 1; i <= 100; i++) {
                currentValue = i*number;
                if(checkNumber(String.valueOf(currentValue)) == SUCCESS){
                    result = String.valueOf(currentValue);
                    break;
                }
            }

            return result;
        }

        private int checkNumber(String n){
            //System.out.println("checkNumber:"+ n);
            int result = FAIL;

            for (int i = 0; i < n.length(); i++) {
                //System.out.println("checkDigit:"+ Character.getNumericValue(n.charAt(i)));
                digits.remove(Character.getNumericValue(n.charAt(i)));
                if(digits.isEmpty()) {
                    result = SUCCESS;
                    break;
                }
            }

            return result;
        }

    }

    static class FastReader {
        private InputStream stream;
        private byte[] buf = new byte[1024];
        private int curChar;
        private int numChars;
        private SpaceCharFilter filter;

        public FastReader(InputStream stream) {
            this.stream = stream;
        }

        int[] readIntArray(int size) throws IOException {
            int[] array = new int[size];
            for (int i = 0; i < size; ++i) {
                array[i] = nextInt();
            }
            return array;
        }

        public int read() {
            if (numChars == -1)
                throw new InputMismatchException();
            if (curChar >= numChars) {
                curChar = 0;
                try {
                    numChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (numChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }

        public int nextInt() {
            int c = read();
            while (isSpaceChar(c))
                c = read();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = read();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = read();
            } while (!isSpaceChar(c));
            return res * sgn;
        }

        public long nextLong() {
            int c = read();
            while (isSpaceChar(c))
                c = read();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = read();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = read();
            } while (!isSpaceChar(c));
            return res * sgn;
        }


        public String nextString() {
            int c = read();
            while (isSpaceChar(c))
                c = read();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = read();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }

        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

    static class FastWriter {
        private PrintWriter writer;

        public FastWriter(OutputStream stream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(stream)));
        }

        public FastWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void println(String x) {
            writer.println(x);
        }

        public void print(String x) {
            writer.print(x);
        }

        public void printSpace() {
            writer.print(" ");
        }

        public void close() {
            writer.close();
        }

    }
}
